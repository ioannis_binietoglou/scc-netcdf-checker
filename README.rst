EARLIENT netcdf format checker
------------------------------

The aim of this script is to check if a netcdf file has the correct 
format to be used with the EARLINET's Single Calculus Chain.

It can check raw lidar data format as well as ancillary file format 
i.e. overlap, sounding, and lidar ratio files.


.. note::
   This script can help you detect some usual mistakes, but does
   not necessarily detect all possible errors in the files. In other
   words, your file might look OK through this script but still give
   errors when used in the SCC. In that case please let me know to add
   the appropriate checks in the specifications.

   
Dependencies
~~~~~~~~~~~~
You need to have the netCDF4 python module installed.

https://pypi.python.org/pypi/netCDF4/


Command line options
~~~~~~~~~~~~~~~~~~~~

Using the -h option in the command line, you can see the following instructions::

   usage: netcdf_checker.py [-h] [-s {data,overlap,lidar_ratio,sounding}]
                         [-l {error,warning,notification}]
                         file
   positional arguments:
      file                  The path of the file to be checked

   optional arguments:
      -h, --help            show this help message and exit
      -s {data,overlap,lidar_ratio,sounding}, --specs {data,overlap,lidar_ratio,sounding}
                        The specificiations to use
      -l {error,warning,notification}, --level {error,warning,notification}
                        The output level

Examples
~~~~~~~~

Check the format of a data file::

   python netcdf_checker.py 20140101bu00.nc

Check the format of a overlap file::

   python netcdf_checker.py ov_20140101bu00.nc -s overlap

Check a data file and print all messages, including notifications::

   python netcdf_checker.py 20140101bu00.nc -l notification